/*global angular */
var app = angular.module("MyProduct", []);

app.controller("ProductController", function ($scope) {
    
    "use strict";
    $scope.items = [
        {id:"One-Way"},
        {id:"Round-Trip"},
        {id:"Multi-City"},
        {id:"World-Tour"}
    ];
    $scope.isHide = true;
    $scope.selectedValue = "";
    $scope.itemCount = 2;
    $scope.entry = {};
    $scope.selectedItem = {};
    $scope.selectItem = function(){
        $scope.isHide = false;
        if($scope.entry){
            $scope.entry = {};
        }
        
        console.log($scope.selectedItem.id);
        if($scope.selectedItem.id == "One-Way"){
            $scope.selectedValue = "col-sm-6";
            $scope.itemCount = 2;
        }
        if($scope.selectedItem.id == "Round-Trip"){
           $scope.selectedValue = "col-sm-4";
            $scope.itemCount = 3;
        }
        if($scope.selectedItem.id == "Multi-City"){
            $scope.selectedValue = "col-sm-4";
            $scope.itemCount = 3;
        }
        if($scope.selectedItem.id == "World-Tour"){
            $scope.selectedValue = "col-sm-3";
            $scope.itemCount = 4;
        }
        
    }
    
    $scope.getItems = function() {
        var retArray = new Array();
        for (var i = 0; i < $scope.itemCount; i++)
            retArray.push(i);
        
        return retArray;
    }
    
    $scope.setValue = function(str, index) {        
        if (index === 0 && $scope.selectedItem.id == "Round-Trip") 
            $scope.entry[2] = str;
    }
    
    
    
});


